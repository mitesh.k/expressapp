create table `email_templates` (
  `id` int unsigned not null auto_increment primary key,
  `email_subject` varchar(255) not null, 
  `email_content` nvarchar(MAX) not null, 
  `created_at` timestamp not null default CURRENT_TIMESTAMP, 
  `updated_at` timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  `is_active` boolean not null default '1'
);

insert into `email_templates`(email_subject,email_content) values('welcome ','<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    Hi {{name}},
    <br>
    <br>
    Thank you for being part of this. Here is the quick start guide for you.
    <br>
    <br>
    {{name}}, reply to this email if you need any help, we are here to help you.
    {{age}}
    {{gender}}
    <br>
    <br>
    Regards,
    <br>
    {{admin}}
</body>

</html>')

create table `email_subjects` (
  `id` int unsigned not null auto_increment primary key,
  `subject_name` varchar(500) not null,
  `subject_content` nvarchar(4000) not null, 
  `created_at` timestamp not null default CURRENT_TIMESTAMP, 
  `updated_at` timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  `is_active` boolean not null default '1'
);

create table `email_templates` (
  `id` int unsigned not null auto_increment primary key,
  `template_name` varchar(500) unique not null,
  `header` nvarchar(4000) not null,
  `content` nvarchar(4000) not null,  
  `footer` nvarchar(4000) not null, 
  `created_at` timestamp not null default CURRENT_TIMESTAMP, 
  `updated_at` timestamp not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  `is_active` boolean not null default '1'
);
insert into email_templates(template_name,header,content,footer) values('welcome','Hi {{name}},','Thank you for being part of this. Here is the quick start guide for you.
    <br>
    <br>
    {{name}}, reply to this email if you need any help, we are here to help you.',' {{admin}}')

insert into email_subject(subject_name,subject_content) values('welcome','WELCOME {{name}}')

insert into email_templates(template_name,header,content,footer) values('register','Hi {{name}},','Thank you for registering of this service. Here is the quick start guide demo for you.
    <br>
    <br>
    {{name}}, reply to this email if you need any help, we are here to help you.',' {{admin}}')

insert into email_subject(subject_name,subject_content) values('register','Register Your Application')

insert into email_templates(template_name,header,content,footer) values('order','Hi {{name}},','your Pizza order is now ready! Please pick it up from Zero Contact pickup/takeaway counter.
    <br>
    <br>
    It was pleasure serving you today.Enjoy your meal.',' {{admin}}')

insert into email_subjects(subject_name,subject_content) values('order','ORDER CONFIRMATION')