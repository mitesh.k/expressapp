const helper = global.helper;
const fsPromise = helper.module.fsPromise;

function readFile (path) {
  return fsPromise.readFile(path,'utf-8');
}

module.exports = readFile;