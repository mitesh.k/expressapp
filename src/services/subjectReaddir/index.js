const helper = global.helper;
const fsPromise = helper.module.fsPromise;
const path = helper.module.path;
const utilities = require('../../utilities');
const config = require('../../../config')

const safePromise = utilities.safePromise;

const SUBJECT_FOLDER = config.SUBJECT_FOLDER;

async function fileLookupSubject () {
  const subjectFolder = path.resolve(SUBJECT_FOLDER);
  const subjectData = fsPromise.readdir(subjectFolder);
  const [err, result] = await safePromise(subjectData);
  if(err) {
    throw err;
  }
  const obj = {};
  result.forEach((item) => {
    const key = item.split('.')[0];  
    obj[key] = {
      subjectPath : item,
    }
  })
  return obj;
}

module.exports = fileLookupSubject;