const helper = global.helper;
const path = helper.module.path;

module.exports = (directoryLocation, fileLocation) => {
  return path.resolve(directoryLocation, fileLocation);
}