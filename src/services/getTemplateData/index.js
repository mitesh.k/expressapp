const { knex } = require('../../../config/db');

function getTemplate (template) {
  return knex.select('header','content','footer').from('email_templates').where('template_name',template);
}

module.exports = getTemplate;