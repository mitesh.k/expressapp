'use strict';

const welcome = require('./welcome');
const send = require('./sendmail');
const sendMail = require('./mailgun-mail');
const readFile = require('./fileread');
const pathResolve = require('./pathresolve');
const sendDB = require('./sendMaildb');
const getTemplate = require('./getTemplateData');
const getSubject = require('./getSubjectData');

module.exports = {
  welcome: welcome,
  send: send,
  senMail: sendMail,
  readFile: readFile,
  pathResolve: pathResolve,
  sendDB: sendDB,
  getTemplate: getTemplate,
  getSubject:getSubject
}