const config = require('../../../config');
const DOMAIN = config.MAILGUN_DOMAIN;
const API_KEY = config.MAILGUN_KEY;

const mailgun = require('mailgun-js')({
  apiKey: API_KEY,
  domain: DOMAIN
});

function sendMail (data) {
  return new Promise((resolve, reject) => {
    mailgun.messages().send(data, function (err,response) {
      if(err) {
        return reject(err);
      }
      resolve(response);
    })
  })
}

module.exports = sendMail;
