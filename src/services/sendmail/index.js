const helper = global.helper;
const underScore = helper.module.underScore;
const config = require('../../../config');
const utilities = require('../../utilities');
const fileLookupContent = require('../contentReaddir');
const fileLookupSubject = require('../subjectReaddir');
const readFile = require('../fileread');
const pathResolve = require('../pathresolve');
const sendMail = require('../mailgun-mail');

const safePromise = utilities.safePromise;

const MAIL_ID = config.MAIL_ID;
const ADMIN = config.ADMIN;
const CONTENT_FOLDER = config.CONTENT_FOLDER;
const SUBJECT_FOLDER = config.SUBJECT_FOLDER;

async function send (payload) {
  
  const [fileErrCont,lookupContent] = await safePromise(fileLookupContent());
  if(fileErrCont) {
    throw fileErrCont;
  }
  const [fileErrSub,lookupSubject] = await safePromise(fileLookupSubject());
  if(fileErrSub) {
    throw fileErrSub;
  }
  const lookupKey = Object.keys(lookupContent);
  
  
  underScore.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
  }
  
  if(lookupKey.includes(payload.template_id)) {
    const fileTemplate = pathResolve(CONTENT_FOLDER, lookupContent[payload.template_id].filePath);
    const [templateErr, templatestr] = await safePromise(readFile(fileTemplate));
    if(templateErr) {
      throw templateErr;
    }
    const fileSubject = pathResolve(SUBJECT_FOLDER, lookupSubject[payload.template_id].subjectPath);
    const [subjectErr, subjectstr] = await safePromise(readFile(fileSubject));
    if(subjectErr) {
      throw subjectErr;
    }
    const html = underScore.template(templatestr)({name:payload.data.name,admin:ADMIN,age:payload.data.age,gender:payload.data.gender});
    const subject = underScore.template(subjectstr)({name:payload.data.name});
    
    const data = {
      from: MAIL_ID,
      to: payload.to,
      subject: subject,
      html: html
    }
    
    const [mailErr, mailResult] = await safePromise(sendMail(data));
    if(mailErr) {
      throw mailErr;
    }
    return mailResult;
  }
}
module.exports = send;
