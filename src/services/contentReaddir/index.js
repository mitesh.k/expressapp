const helper = global.helper;
const fsPromise = helper.module.fsPromise;
const path = helper.module.path;
const utilities = require('../../utilities');
const config = require('../../../config')

const safePromise = utilities.safePromise;
const CONTENT_FOLDER = config.CONTENT_FOLDER;


async function fileLookupContent () {
  const contentFolder = path.resolve(CONTENT_FOLDER);
  const contentData = fsPromise.readdir(contentFolder);
  const [err, result] = await safePromise(contentData);
  if(err) {
    throw err;
  }
  const obj = {};
  result.forEach((item) => {
    const key = item.split('.')[0];  
    obj[key] = {
      filePath : item
    }
  })
  return obj;
}

module.exports = fileLookupContent;

