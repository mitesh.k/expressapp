const helper = global.helper;
const underScore = helper.module.underScore;
const config = require('../../../config');
const utilities = require('../../utilities');

const sendMail = require('../mailgun-mail');
const getTemplate = require('../getTemplateData');
const getSubject = require('../getSubjectData');
const fileRead = require('../fileread');
const pathResolve = require('../pathresolve');

const safePromise = utilities.safePromise;


const MAIL_ID = config.MAIL_ID;
const ADMIN = config.ADMIN;
const RAW_TEMPLATE = config.RAW_TEMPLATE;
const TEMPLATE_DIRECTORY = config.TEMPLATE_DIRECTORY

async function sendDB (payload) {
  
  underScore.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
  }
  
  const [dbTemplateError, dbTemplateResult] = await safePromise(getTemplate(payload.template_id));
  if(dbTemplateError) {
    throw dbTemplateError;
  }
  
  const fileTemplate = pathResolve(TEMPLATE_DIRECTORY, RAW_TEMPLATE);
  const [templateErr, templatestr] = await safePromise(fileRead(fileTemplate));
  if(templateErr) {
    throw templateErr;
  }
  
  const rawHtml = underScore.template(templatestr)({header:dbTemplateResult[0].header,content:dbTemplateResult[0].content,footer:dbTemplateResult[0].footer});
  const htmlResult = underScore.template(rawHtml)({name:payload.data.name,admin:ADMIN});
  
  const[dbSubjectError, dbSubjectResult] = await safePromise(getSubject(payload.template_id));
  if(dbSubjectError) {
    throw dbSubjectError;
  }
  
  const subject = underScore.template(dbSubjectResult[0].subject_content)({name:payload.data.name});
  
  const data = {
    from: MAIL_ID,
    to: payload.to,
    subject: subject,
    html: htmlResult
  }
  
  const [mailErr, mailResult] = await safePromise(sendMail(data));
  if(mailErr) {
    throw mailErr;
  }
  return mailResult;
  
}
module.exports = sendDB;