const { knex } = require('../../../config/db');

function getSubject (subject) {
  return knex.select('subject_content').from('email_subjects').where('subject_name',subject);
}

module.exports = getSubject;