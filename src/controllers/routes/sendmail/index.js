const express = require('express');
const router = express.Router();
const middlewares = require('../../../middlewares');
const services = require('../../../services');
const rules = require('../../../../rules');
const utilites = require('../../../utilities');

const validate = middlewares.validate;
const sanitize = middlewares.sanitize;
const safePromise = utilites.safePromise;
const send = services.send;


router
  .route('/send')
  // eslint-disable-next-line no-unused-vars
  .post(sanitize, validate( rules.send ), async (req, res, next) => {
    const body = req.payload;
    const [err, data] = await safePromise(send(body));
    if(err) {
      return res.status(500).json({
        status: 'something error occured',
        message: err
      })
    }
    res.json({
      status: "ok, mail send successfully check your mail",
      res:{
        data
      }
    })
  });

module.exports = router;