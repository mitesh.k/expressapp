const express = require('express');
const router = express.Router();
const middlewares = require('../../../middlewares');
const services = require('../../../services');
const rules = require('../../../../rules');
const utilites = require('../../../utilities');

const validate = middlewares.validate;
const sanitize = middlewares.sanitize;
const safePromise = utilites.safePromise;

const sendDB = services.sendDB;


router
  .route('/sendDB')
  // eslint-disable-next-line no-unused-vars
  .post(sanitize, validate( rules.send ), async (req, res, next) => {
    const body = req.payload;
    const [error, data] = await safePromise(sendDB(body));
    if(error) {
      return res.status(500).json({
        status: "error sending in mail"
      });
    }
    res.json({
      message: "mail has sent check your mail",
      data
    });
  });

module.exports = router;