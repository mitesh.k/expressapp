'use strict'
const utilites = require("../../utilities");
const logger = utilites.logger;

const log = logger('sanitize');
const functionName = "sanitize-middleware";

let sanitize = (req, res, next) => {
  const payload = req.body;
  
  const sanitized_payload = {};
  log.info(functionName, "Incoming Headers", JSON.stringify(req.headers));
  log.info(functionName, "Req Body", JSON.stringify(payload));

  //Sanitize input payload - Example
  if (payload.Template_id || payload.template_id) {
    sanitized_payload.template_id = (payload.Template_id || payload.template_id).toString().trim();
  }
  if (payload.To || payload.to) {
    sanitized_payload.to = (payload.To || payload.to).toString().trim();
  }
  if (payload.Data || payload.data) {
    sanitized_payload.data = (payload.Data || payload.data);
  }
  req.payload = sanitized_payload;

  next();
}


module.exports = sanitize;