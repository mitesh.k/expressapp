const express = require('express');
const cookieParser = require('cookie-parser');
const Joi = require('joi');
const path = require('path');
const fs = require('fs');
const fsPromise = require('fs/promises');
const underScore = require('underscore');

module.exports = {
  express,
  cookieParser,
  Joi,
  path,
  fs,
  underScore,
  fsPromise
}